from django.conf.urls import url, include
from django.contrib import admin
from pet.routers import router as cat_router

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^api/', include(cat_router.urls, namespace='cats')),
    url(r'^', include('webapp.urls'))
]
