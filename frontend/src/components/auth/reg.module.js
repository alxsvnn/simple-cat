'use strict';

import {headers} from './auth.settings';

const MODULE_NAME = 'myReg';

export default angular
  .module(MODULE_NAME,['ngMaterial', 'ngMessages'])
  .component(MODULE_NAME, {
    template: require('./reg.template.html'),
    controllerAs: MODULE_NAME
  })
  .controller('registrationCtrl', ['$scope', 'Registration',
    function ($scope, Registration) {

      $scope.regForm = {
        username: '',
        password: '',
        email: ''
      };

      $scope.submit = function () {
        let form = $scope.regForm;
        if (form.$valid) {
          Registration.request(form);
        }
      };
    }])
  .service('Registration', ['$cookies', '$location', '$http',
    function ($cookies, $location, $http) {
      this.request = function (data) {
        $http({
          method: 'POST',
          url: '/api/user/registration/',
          headers: headers,
          data: {
            username: data.username.$modelValue,
            password: data.password.$modelValue,
            email: data.email.$modelValue
          }
        }).then(
          function (resp) {
            $location.path('/login')
          },
          function (err) {
            alert('Что-то пошло не так')
          }
        )
      }
  }]);
