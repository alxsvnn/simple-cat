'use strict';

import {headers} from './auth.settings';

const MODULE_NAME = 'myAuth';

export default angular
  .module(MODULE_NAME, [])
  .component(MODULE_NAME, {
    template: require('./auth.template.html'),
    controllerAs: MODULE_NAME
    })
  .controller('loginCtrl', [
    '$rootScope', '$scope', '$http', '$location', 'loginAuthSet', '$cookies',
    function ($rootScope, $scope, $http, $location, loginAuthSet, $cookies) {

      // пременные формы аутеинтификаии
      $scope.loginForm = {
        username: '',
        password: ''
      };

      // ф-ция получения актуального client_id
      $scope.clientId = function () {
        $http.get('api/client_id/', { headers: headers })
          .then(
            function (resp) {
              $rootScope.clientId = resp.data.clientId;
            }, null
          );
      };
      // если приложение только что загрузилось => запрос client_id
      if (!$rootScope.clientId) $scope.clientId();

      // ф-ция входа
      $scope.login = function () {
        $http({
          method: 'POST',
          url: '/api/o/token/',
          headers: headers,
          data: JSON.stringify({
            grant_type: 'password',
            username: $scope.loginForm.username.$viewValue,
            password: $scope.loginForm.password.$viewValue,
            client_id: $rootScope.clientId
          })
        }).then(
          function (resp) {
            loginAuthSet.success(resp);
            $location.path('/');
          },
          function (err) {
            if (err.status === 401) {
              $scope.clientId();
              $scope.login();
            }
            loginAuthSet.removeTrash();
          }
        )
      };

      // запрос удаления тонкена + почистить куки
      $scope.logout = function () {
        $http({
          method: 'POST',
          url: '/api/o/revoke_token/',
          headers: headers,
          data: {
            client_id: $rootScope.clientId,
            token: $cookies.get('token')
          }
        });
        loginAuthSet.removeTrash();
        $location.path('/login');
      };

      // 'logout' извне
      $scope.$on('logout', function () {
        loginAuthSet.logout();
      });

    }])
  .service('loginAuthSet',
    ['$rootScope', '$cookies', '$location', '$http',
      function ($rootScope, $cookies, $location, $http) {
        let t = this;
        // положить все в куки на будущее
        t.success = function (resp) {
          if (resp.status !== 200) return;
          $rootScope.isAuth = true;
          $cookies.put('auth', true);
          $cookies.put('token', resp.data.access_token);
        };

        t.logout = function () {
          $http.post('api/o/revoke_token/', {
            headers: headers,
            client_id: $rootScope.clientId
          });
          t.removeTrash();
          $location.path('/')
        };

        // чистка после logout'а или неудачного login'а
        t.removeTrash = function () {
          $rootScope.isAuth = false;
          $cookies.put('auth', false);
          $cookies.remove('token');
        }

      }]);
