'use strict';

export default angular
  .module('myLogout', [])
  .controller('logoutCtrl', ['$scope', function ($scope) {
    $scope.$broadcast('logout');
  }])
