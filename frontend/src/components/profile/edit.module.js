'use strict';

const MODULE_NAME = 'myProfileEdit';

export default angular
  .module(MODULE_NAME, ['ngMaterial', 'ngMessages'])
  .controller('profileEditCtrl', [
    '$rootScope', '$scope', '$http', '$location', '$routeParams',
    function ($rootScope, $scope, $http, $location, $routeParams) {

      $scope.catForm = {
        id: '',
        name: '',
        age: '',
        cat_type: '',
        characteristic: ''
      };

      $scope.catType = {
        'per': 'Длинношёрстный',
        'bml': 'Полудлинношерстный',
        'drx': 'Короткошёрстный',
        'okh': 'Сиамо-Ориентальный короткошёрстный'
      };

      if ($routeParams.id !== 'add') {
        // запрос данных кота
        $http.get('api/cats/' + $routeParams.id + '/', {
          headers: $rootScope.headers()
        }).then(
          function (resp) {
            // установка ответа в форму
            for (let field in resp.data) {
              if (field !== 'id') {
                $scope.catForm[field].$setViewValue(resp.data[field]);
                $scope.catForm[field].$render();
              }
            }
          },
          function (err) {
            if (err.status === 401) $scope.$broadcast('logout');
            $location.path('/login')
          }
        );
      }

      // готовим данные
      $scope.buildData = function () {
        return {
          name: $scope.catForm.name.$modelValue,
          age: $scope.catForm.age.$modelValue,
          cat_type: $scope.catForm.cat_type.$modelValue,
          characteristic: $scope.catForm.characteristic.$modelValue
        }
      };

      // создание кота
      $scope.submit = function () {
        let t = this;
        // определение метода => выбор url'а
        if ($routeParams.id !== 'add') {
          t.method = 'PUT';
          t.url = 'api/cats/' + $routeParams.id + '/';
        } else if ($routeParams.id === 'add') {
          t.method = 'POST';
          t.url = 'api/cats/';
        }
        // создание или обновление
        $http({
          method: t.method,
          url: t.url,
          headers: $rootScope.headers(),
          data: $scope.buildData()
        })
          .then(
            function (resp) {
              $location.path('/profile');
            },
            function (err) {
              console.log(err)
              alert('Что-то пошло не так...')
            }
          )
      }

    }
  ]);
