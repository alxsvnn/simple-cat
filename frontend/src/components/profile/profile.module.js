'use strict';

const MODULE_NAME = 'myProfile';

export default angular
  .module(MODULE_NAME, [])
  .component(MODULE_NAME, {
    template: require('./profile.template.html'),
    controllerAs: MODULE_NAME
  })
  .controller('profileCtrl', [
    '$rootScope', '$scope', '$http', '$mdDialog', '$location', '$route',
    function ($rootScope, $scope, $http, $mdDialog, $location, $route) {
      let t = this;
      // запрос списка котов
      $http.get('api/cats/', {
        headers: $rootScope.headers()
      }).then(
        function (resp) {
          $scope.cats = resp.data;
        },
        function (err) {
          if (err.status === 401) $scope.$broadcast('logout');
          $location.path('/login')
        }
      );

      // подтверждение удаления
      $scope.askDelete = function (cat_id) {
        let delDialog = $mdDialog.confirm()
          .parent(angular.element(document.getElementById('popUpContainer')))
          .clickOutsideToClose()
          .title('Oooouuuch!')
          .textContent('Не удаляй меня, я не хочу!')
          .cancel('Ну ладно')
          .ok('Удалить');
        $mdDialog.show(delDialog).then(
          function () {
            t.deleteCat(cat_id);
            $route.reload();
          },
          null
        );
      };

      // удаление кто :'(
      t.deleteCat = function (cat_id) {
        $http.delete('api/cats/' + cat_id + '/', {
          headers: $rootScope.headers()
        });
      };

      // переход на страницу редактирования профиля кота
      $scope.changeCat = function (cat_id) {
        $location.path('/profile/' + cat_id);
      };

      // переход на страницу создания новойго кота
      $scope.addCat = function () {
        $location.path('/profile/add')
      }

    }])
