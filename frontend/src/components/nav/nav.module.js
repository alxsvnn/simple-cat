'use strict';

const MODULE_NAME = 'myNav';

export default angular
  .module(MODULE_NAME, [])
  .component(MODULE_NAME, {
    template: require('./nav.template.html'),
    controllerAs: MODULE_NAME
  });
