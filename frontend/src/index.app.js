'use strict';

import angular from 'angular';
import ngRoute from 'angular-route';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngMaterial from 'angular-material';
import ngMessages from 'angular-messages';
import ngAnimate from 'angular-animate';
import myAuth from './components/auth/auth.module';
import myLogout from './components/auth/logout.module';
import myReg from './components/auth/reg.module';
import myNav from './components/nav/nav.module';
import myProfile from './components/profile/profile.module';
import myProfileEdit from './components/profile/edit.module';
import './base.css'


const MODULE_NAME = 'app';

angular
  .module(MODULE_NAME, [
    ngRoute,
    ngMaterial,
    ngMessages,
    ngAnimate,
    ngResource,
    ngCookies,
    myAuth.name,
    myLogout.name,
    myReg.name,
    myNav.name,
    myProfile.name,
    myProfileEdit.name
])
  .run([
    '$rootScope', '$cookies',
    function ($rootScope, $cookies) {
      // заголовок с токеном после авторизации
      $rootScope.headers = function () {
        let t = this;
        t.header = {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*',
        };
        if ($cookies.get('token')) t.header.Authorization = 'Bearer ' + $cookies.get('token');
        return t.header
      };
      /*
        * важный и нужный параметр для авторизации, удаления токена и т.п.
        * здесь он просто определяетя => запрос происходит позже в модуле
        * авторизации
      */
      $rootScope.clientId = NaN;

      // глобальный isAuth
      if ($cookies.get('auth') && $cookies.get('token')) {
        $rootScope.isAuth = true;
      } else {
        $rootScope.isAuth = false;
      }
    }])
  .directive(MODULE_NAME, function () {
    return {
      template: '<my-nav></my-nav>\n' +
      '<md-conteiner ng-view></md-conteiner>',
      controllerAs: 'AppCtrl'
    }
  })
  .config(['$routeProvider', function ($routeProvider) {
    // angular-router
    $routeProvider
      .when('/', {
        template: require('./templates/app.template.html')
      })
      .when('/login', {
        template: require('./components/auth/auth.template.html'),
        controller: 'loginCtrl'
      })
      .when('/logout', {
        template: require('./components/auth/logout.template.html'),
        controller: 'logoutCtrl'
      })
      .when('/registration', {
        template: require('./components/auth/reg.template.html'),
        controller: 'registrationCtrl'
      })
      .when('/profile', {
        template: require('./components/profile/profile.template.html'),
      })
      .when('/profile/:id', {
        template: require('./components/profile/edit.template.html'),
      })
      .when('/404', {
        template: require('./templates/404.template.html')
      })
      .otherwise({
        redirectTo: '/404'
      });
  }]);
