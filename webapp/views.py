from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from oauth2_provider.models import Application
from .serializers import UserRegistrationSerializer


class UserRegistrationViewSet(APIView):
    """
      Обработка запроса из формы регистрации.
      Принимает только POST запросы.
      Простая сериализация, валидация, регистрация пользователя.
    """

    http_method_names = ('post',)
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        serialize = UserRegistrationSerializer(data=request.data)
        serialize.is_valid(raise_exception=True)
        # нет ошибок => создать нового пользователя
        user = User.objects.create_user(
            username=serialize.data.get('username'),
            password=serialize.data.get('password'),
            email=serialize.data.get('email')
        )
        return Response(status=status.HTTP_201_CREATED)


class ClientIdViewSet(APIView):
    """
      При загрузке приложение обращается к серверу
      для получения актуального client_id
    """
    http_method_names = ('get',)
    permission_classes = (AllowAny,)

    def get(self, request, *args, **kwargs):
        """
          Здесь client_id берется без заморочек,
          т.к. это все есть _пример_
        """
        client_id = Application.objects.last().client_id
        return Response({'clientId': client_id}, status=status.HTTP_200_OK)
