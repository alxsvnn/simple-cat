from django.conf.urls import url
from django.views.generic import TemplateView
from .views import UserRegistrationViewSet, ClientIdViewSet


urlpatterns = [
    url(r'^api/user/registration/$',
        UserRegistrationViewSet.as_view(),
        name='user-registration'),
    url(r'^api/client_id/$',
        ClientIdViewSet.as_view(), name='client_id'),
    url(r'^$',
        TemplateView.as_view(template_name='webapp/index.html'),
        name='index'),
]

