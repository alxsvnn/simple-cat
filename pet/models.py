from django.db import models
from django.utils.translation import ugettext as _
from django_userforeignkey.models.fields import UserForeignKey
from django.core.validators import MinValueValidator, MaxValueValidator


class Cat(models.Model):
    """ Коты имеют имя, возраст, породу и харакерстику """

    WCF_TYPE = (
        ('per', 'Длинношёрстный'),
        ('bml', 'Полудлинношерстный'),
        ('drx', 'Короткошёрстный'),
        ('okh', 'Сиамо-Ориентальный короткошёрстный')
    )

    name = models.CharField(verbose_name=_('Кличка'), max_length=60)
    age = models.PositiveSmallIntegerField(
        verbose_name=_('Возраст'), blank=True, null=True,
        validators=[MinValueValidator(0), MaxValueValidator(30)]
    )
    cat_type = models.CharField(
        verbose_name=_('Группа'), max_length=3, choices=WCF_TYPE,
        blank=True, null=True
    )
    characteristic = models.TextField(
        verbose_name=_('Особые приметы'), max_length=2000,
        blank=True, null=True
    )
    user = UserForeignKey(auto_user_add=True)

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Cat, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        ordering = ('name',)
        verbose_name = _('кота')
        verbose_name_plural = _('коты')
