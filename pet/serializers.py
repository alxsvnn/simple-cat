from rest_framework import serializers
from .models import Cat


class CatSerializer(serializers.ModelSerializer):
    """ Сериализатор модели Котов """

    class Meta:
        model = Cat
        fields = ('name', 'age', 'cat_type', 'characteristic', 'id')
