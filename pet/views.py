from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, OAuth2Authentication
from .serializers import CatSerializer
from .models import Cat


class CatViewSet(ModelViewSet):
    """ Представлении модели Котов """

    queryset = Cat.objects.all()
    serializer_class = CatSerializer

    def get_queryset(self):
        qs = super(CatViewSet, self)\
          .get_queryset().filter(user=self.request.user)
        return qs
