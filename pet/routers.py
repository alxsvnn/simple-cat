from rest_framework import routers
from .views import CatViewSet


router = routers.DefaultRouter()
router.register(r'cats', CatViewSet, base_name='cats')
